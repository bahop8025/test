import { Route, Routes } from 'react-router-dom';
import Layouts from './components/Layouts/Layouts';
import NotFound from './components/NotFound/NotFound';
import DetailProduct from './components/detailProduct/DetailProduct';
import Test from './pages/Test';
import Home from './pages/home/Home';
import RealEstateForRent from './pages/realEstateForRent/RealEstateForRent';
import Realestateforsale from './pages/realEstateForSale/Realestateforsale';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layouts />}>
        {/* ----------------NotFound------------ */}
        <Route path="*" element={<NotFound />} />
        {/* ----------------home------------ */}
        <Route>
          <Route path="/" element={<Home />} />
          <Route path="/nha-dat-ban" element={<Realestateforsale />} />
          <Route path="/nha-dat-ban/:slug" element={<DetailProduct />} />
          {/* ----------- */}
          <Route path="/nha-dat-cho-thue" element={<RealEstateForRent />} />
          <Route path="/nha-dat-cho-thue/:slug" element={<DetailProduct />} />

          <Route path="/test" element={<Test />} />
        </Route>
        {/* ------------- employee------------- */}
        <Route></Route>
      </Route>
    </Routes>
  );
}

export default App;
