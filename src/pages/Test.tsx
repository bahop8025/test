import { useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/navigation';
import 'swiper/css/thumbs';
import './test.css';
import { FreeMode, Navigation, Thumbs } from 'swiper/modules';
import styles from './tests.module.css';
import MapDetails from '../components/detailProduct/mapDetails/MapDetails';
import ShortInfo from '../components/detailProduct/shortInfo/ShortInfo';
import Infomation from '../components/detailProduct/infomation/Infomation';
import SidebarDetail from '../components/detailProduct/sidebarDetail/SidebarDetail';
import { Link } from 'react-router-dom';

export default function Test() {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  return (
    <section>
      <div className={styles.containers}>
        <div>
          <Swiper
            style={{
              '--swiper-navigation-color': '#fff',
              '--swiper-pagination-color': '#fff'
            }}
            loop={true}
            spaceBetween={10}
            navigation={true}
            thumbs={thumbsSwiper ? { swiper: thumbsSwiper } : undefined}
            modules={[FreeMode, Navigation, Thumbs]}
            className={styles.mySwiper2222}
          >
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-1.jpg" alt="Nature 1" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-2.jpg" alt="Nature 2" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-3.jpg" alt="Nature 3" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-4.jpg" alt="Nature 4" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-5.jpg" alt="Nature 5" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-6.jpg" alt="Nature 6" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-7.jpg" alt="Nature 7" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-8.jpg" alt="Nature 8" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-9.jpg" alt="Nature 9" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-10.jpg" alt="Nature 10" />
            </SwiperSlide>
          </Swiper>
          <Swiper
            onSwiper={setThumbsSwiper}
            loop={true}
            spaceBetween={10}
            slidesPerView={4}
            freeMode={true}
            watchSlidesProgress={true}
            modules={[FreeMode, Navigation, Thumbs]}
            className={styles.mySwiper}
          >
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-1.jpg" alt="Nature 1" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-2.jpg" alt="Nature 2" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-3.jpg" alt="Nature 3" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-4.jpg" alt="Nature 4" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-5.jpg" alt="Nature 5" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-6.jpg" alt="Nature 6" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-7.jpg" alt="Nature 7" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-8.jpg" alt="Nature 8" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-9.jpg" alt="Nature 9" />
            </SwiperSlide>
            <SwiperSlide>
              <img src="https://swiperjs.com/demos/images/nature-10.jpg" alt="Nature 10" />
            </SwiperSlide>
          </Swiper>
          <div className={styles.breadcrumb}>
            <Link to="">Bán</Link>
            <span>/</span>
            <Link to="">Hồ Chí Minh</Link>
            <span>/</span>
            <Link to="">Quận 10</Link>
            <span>/</span>
            <Link to="">Nhà mặt phố tại phố Lý Thường Kiệt</Link>
          </div>
          <div className={styles.info}>
            <h1>Bán tòa nhà văn phòng đường Lý Thường Kiệt, phường 14, quận 10. DT 20x16m (hầm 8 tầng) chỉ 75 tỷ</h1>
            <span>
              Bán tòa nhà văn phòng đường Lý Thường Kiệt, phường 14, quận 10. DT 20x16m (hầm 8 tầng) chỉ 75 tỷ
            </span>
            <div className={styles.shortInfo}>
              <div>
                <div>
                  <span className={styles.title}>Mức giá</span>
                  <span className={styles.value}>75 tỷ</span>
                  <span className={styles.ext}>~234,38 triệu/m²</span>
                </div>
                <div>
                  <span className={styles.title}>Diện tích</span>
                  <span className={styles.value}>320 m²</span>
                  <span className={styles.ext}>8 tầng</span>
                </div>
              </div>
              <div>
              </div>
            </div>
            <div className={styles.productDetail}>
              <span>Thông tin mô tả</span>
              <div>
                <ul>
                  <li>Sản phẩm tốt có đủ tất cả ưu điểm còn sót lại trung tâm quận 10:</li>
                  <li>Cần tiền bán gấp tòa nhà văn phòng đường Lý Thường Kiệt, Phường 14, Quận 10.</li>
                  <li>- Diện tích: 20 x 16m, công nhận đủ: 320m.</li>
                  <li>- Diện tích sàn: 2900m.</li>
                </ul>
              </div>
            </div>
            <div className={styles.productDetail}>
              <span>Đặc điểm bất động sản</span>
              <div className={styles.specsContent}>
                <div>
                  <span>Diện tích</span>
                  <span>320 m²</span>
                </div>
                <div>
                  <span>Mức giá</span>
                  <span>95 tỷ</span>
                </div>
                <div>
                  <span>Số tầng</span>
                  <span>7 tầng</span>
                </div>
              </div>
            </div>
          </div>
          <div>
            <MapDetails />
          </div>
          <div>
            <ShortInfo />
          </div>
        </div>
        <div className={styles.rigth}>
          <Infomation />
          <SidebarDetail />
          <SidebarDetail />
        </div>
      </div>
    </section>
  );
}
