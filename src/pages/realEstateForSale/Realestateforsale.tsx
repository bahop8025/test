import { SlArrowDown } from 'react-icons/sl';
import { Link } from 'react-router-dom';
import Tiles from '../../components/Titles/Tiles';
import Buttons from '../../components/filed/Buttons';
import Products from '../../components/products/Products';
import styles from './realestateforsale.module.css';

export default function Realestateforsale() {
  return (
    <div>
      <section className={styles.produst}>
        <div className="container">
          <div className={styles.titles}>
            <Tiles texts="Bất động sản dành cho bạn" fontSize={27} fontWeight={500} />
            <span>
              <Link to="">Tin nhà đất bán mới nhất</Link> | <Link to=""> Tin nhà đất cho thuê mới nhất</Link>
            </span>
          </div>
          <div className={styles.wap}>
            <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
            <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
            <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
            <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
            <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
            <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
            <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
          </div>
          <div className={styles.showMore}>
            <Buttons texts="Mở rộng" icons={<SlArrowDown />} />
          </div>
        </div>
      </section>
    </div>
  );
}
