import { DatePicker } from 'antd';
import styles from './stylesDate.module.css';

export default function Dates(props) {
  const { defaultValue, onChange, labels, customClass, required } = props;
  return (
    <div className={`${styles.dates} ${customClass}`}>
      {labels && (
        <label htmlFor="label">
          {labels}: {required && <span>*</span>}
        </label>
      )}
      <DatePicker format="DD-MM-YYYY" defaultValue={defaultValue} onChange={onChange} />
    </div>
  );
}
