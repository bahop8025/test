import { DatePicker } from 'antd';
import styles from './stylesDate.module.css';

export default function DateTimes(props) {
  const { labels, customClass, required, defaultValue, onChange } = props;

  return (
    <div className={`${styles.dates} ${customClass}`}>
      {labels && (
        <label htmlFor="label">
          {labels}: {required && <span>*</span>}
        </label>
      )}
      <DatePicker format="DD-MM-YYYY hh:mm" defaultValue={defaultValue} showTime onChange={onChange} />
    </div>
  );
}
