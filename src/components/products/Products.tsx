import { FcLike } from 'react-icons/fc';
import { IoLocationOutline } from 'react-icons/io5';
import { Link } from 'react-router-dom';
import styles from './products.module.css';

export default function Products(props) {
  const { links } = props;
  return (
    <Link to={links}>
      <section className={styles.products}>
        <img src="../../../public/images/product.jpg" alt="pro" />
        <div className={styles.infomation}>
          <h3>Bán siêu tòa nhà Lý Thường Kiệt, P 14, Q10. DT: 15x20m. HĐT 170tr/th. Giá 58 tỷ</h3>
          <div className={styles.cart}>
            <span>58 tỷ</span>
            <span>192,1 m²</span>
          </div>
          <div className={styles.location}>
            <span>
              <IoLocationOutline />
              Quận 10, Hồ Chí Minh
            </span>
          </div>
          <div className={styles.interact}>
            <span>Đăng hôm nay</span>
            <span>
              <FcLike />
            </span>
          </div>
        </div>
      </section>
    </Link>
  );
}
