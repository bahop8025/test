import styles from './loading.module.css';
import { useEffect, useState } from 'react';

export default function Loading() {
  const [isDarkBackground, setIsDarkBackground] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsDarkBackground(true);
    }, 6000); // Change background color after 5 seconds

    return () => clearTimeout(timer); // Cleanup timer on unmount
  }, []);

  return (
    <div className={`${styles.overlay} ${isDarkBackground ? styles.dark : ''}`}>
      <div>
        <div className={styles.loader}></div>
      </div>
    </div>
  );
}
