import { Outlet } from 'react-router-dom';
import Footers from '../footer/Footers';
import Headers from '../header/Headers';

export default function Layouts() {
  return (
    <>
      <Headers />
      <Outlet />
      <Footers />
    </>
  );
}
