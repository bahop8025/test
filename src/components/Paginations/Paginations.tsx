import { Pagination } from 'antd';

export default function Paginations(props) {
  const { totals } = props;
  return <Pagination total={totals} showSizeChanger showQuickJumper showTotal={(total) => `Total ${total} items`} />;
}
