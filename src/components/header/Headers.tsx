import { Header } from 'antd/es/layout/layout';
import styles from './headers.module.css';
import Information from './Information/Information';
import Logos from './Logos/Logos';
import Menus from './Menus/Menus';
import { useEffect, useState } from 'react';

export default function Headers() {
  const [isFixed, setIsFixed] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 70) {
        setIsFixed(true);
      } else {
        setIsFixed(false);
      }
    };

    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <Header className={`${styles.headers} ${isFixed ? styles.fixed : ''}`}>
      <Logos />
      <Menus />
      <Information />
    </Header>
  );
}
