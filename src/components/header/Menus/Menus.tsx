import { Link, NavLink } from 'react-router-dom';
import styles from './menus.module.css';

export default function Menus() {
  return (
    <div className={styles.menus}>
      <ul>
        <li>
          <NavLink className={({ isActive }) => (isActive ? styles.active : '')} to="nha-dat-ban">
            Nhà đất bán
          </NavLink>
        </li>
        <li>
          <NavLink className={({ isActive }) => (isActive ? styles.active : '')} to="nha-dat-cho-thue">
            Nhà đất cho thuê
          </NavLink>
        </li>
      </ul>
    </div>
  );
}
