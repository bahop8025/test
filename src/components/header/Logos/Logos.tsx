import { Link } from 'react-router-dom';
import styles from './logos.module.css';

export default function Logos() {
  return (
    <Link to="/" className={styles.logos}>
      <img src="/images/avt-logo.png" alt="images" />
    </Link>
  );
}
