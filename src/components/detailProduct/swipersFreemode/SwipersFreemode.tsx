import 'swiper/css';
import 'swiper/css/pagination';
import { Pagination } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import Products from '../../products/Products';
import './swiper.css';
import styles from './swipersFreemode.module.css';

export default function SwipersFreemode() {
  return (
    <>
      <Swiper
        slidesPerView={1}
        spaceBetween={10}
        pagination={{
          clickable: false
        }}
        breakpoints={{
          '@0.00': {
            slidesPerView: 1,
            spaceBetween: 10
          },
          '@0.75': {
            slidesPerView: 2,
            spaceBetween: 20
          },
          '@1.00': {
            slidesPerView: 3,
            spaceBetween: 40
          },
          '@1.50': {
            slidesPerView: 4,
            spaceBetween: 50
          }
        }}
        modules={[Pagination]}
        className={styles.swipersFreemode}
      >
        <SwiperSlide>
          <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
        </SwiperSlide>
        <SwiperSlide>
          <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
        </SwiperSlide>
        <SwiperSlide>
          <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
        </SwiperSlide>
        <SwiperSlide>
          <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
        </SwiperSlide>
        <SwiperSlide>
          <Products links="4-Điểm-Mới-Của-Luật-Đất-Đai-2024-Tác-Động-Tích-Cực-Đến-Doanh-Nghiệp-BĐS" />
        </SwiperSlide>
      </Swiper>
    </>
  );
}
