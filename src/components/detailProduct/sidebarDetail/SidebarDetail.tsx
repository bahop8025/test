import { Link } from 'react-router-dom';
import styles from './sidebarDetail.module.css';

export default function SidebarDetail() {
  return (
    <section className={styles.sidebarDetail}>
      <h2>Bán nhà mặt phố tại Quận 10</h2>
      <ul>
        <li>
          <h3>
            <Link to="">Phường 12 (330)</Link>
          </h3>
        </li>
        <li>
          <h3>
            <Link to="">Phường 14 (136)</Link>
          </h3>
        </li>
        <li>
          <h3>
            <Link to="">Phường 15 (133)</Link>
          </h3>
        </li>
        <li>
          <h3>
            <Link to="">Phường 10 (57)</Link>
          </h3>
        </li>
        <li>
          <h3>
            <Link to="">Phường 9 (49)</Link>
          </h3>
        </li>
      </ul>
    </section>
  );
}
