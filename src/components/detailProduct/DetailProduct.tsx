import { CiWarning } from 'react-icons/ci';
import { Link } from 'react-router-dom';
import styles from './detailProduct.module.css';
import Infomation from './infomation/Infomation';
import MapDetails from './mapDetails/MapDetails';
import ShortInfo from './shortInfo/ShortInfo';
import SidebarDetail from './sidebarDetail/SidebarDetail';
import Swipers from './swipers/Swipers';
import ProductYou from './productYou/ProductYou';
import Description from './description/Description';

export default function DetailProduct() {
  return (
    <section className="container">
      <div className={`${styles.details}`}>
        <div className={styles.left}>
          <Swipers />
          <div className={styles.breadcrumb}>
            <Link to="">Bán</Link>
            <span>/</span>
            <Link to="">Hồ Chí Minh</Link>
            <span>/</span>
            <Link to="">Quận 10</Link>
            <span>/</span>
            <Link to="">Nhà mặt phố tại phố Lý Thường Kiệt</Link>
          </div>
          <div className={styles.info}>
            <h1>Bán tòa nhà văn phòng đường Lý Thường Kiệt, phường 14, quận 10. DT 20x16m (hầm 8 tầng) chỉ 75 tỷ</h1>
            <span>
              Bán tòa nhà văn phòng đường Lý Thường Kiệt, phường 14, quận 10. DT 20x16m (hầm 8 tầng) chỉ 75 tỷ
            </span>
            <div className={styles.shortInfo}>
              <div>
                <div>
                  <span className={styles.title}>Mức giá</span>
                  <span className={styles.value}>75 tỷ</span>
                  <span className={styles.ext}>~234,38 triệu/m²</span>
                </div>
                <div>
                  <span className={styles.title}>Diện tích</span>
                  <span className={styles.value}>320 m²</span>
                  <span className={styles.ext}>8 tầng</span>
                </div>
              </div>
              <div>
                <CiWarning />
              </div>
            </div>
            <div className={styles.productDetail}>
              <span>Thông tin mô tả</span>
              <div>
                <ul>
                  <li>Sản phẩm tốt có đủ tất cả ưu điểm còn sót lại trung tâm quận 10:</li>
                  <li>Cần tiền bán gấp tòa nhà văn phòng đường Lý Thường Kiệt, Phường 14, Quận 10.</li>
                  <li>- Diện tích: 20 x 16m, công nhận đủ: 320m.</li>
                  <li>- Diện tích sàn: 2900m.</li>
                </ul>
              </div>
            </div>
            <div className={styles.productDetail}>
              <span>Đặc điểm bất động sản</span>
              <div className={styles.specsContent}>
                <div>
                  <CiWarning />
                  <span>Diện tích</span>
                  <span>320 m²</span>
                </div>
                <div>
                  <CiWarning />
                  <span>Mức giá</span>
                  <span>95 tỷ</span>
                </div>
                <div>
                  <CiWarning />
                  <span>Số tầng</span>
                  <span>7 tầng</span>
                </div>
              </div>
            </div>
          </div>
          <div>
            <MapDetails />
          </div>
          <div>
            <ShortInfo />
          </div>
          <div>
            <ProductYou />
          </div>
          <Description />
        </div>
        <div className={styles.rigth}>
          <Infomation />
          <SidebarDetail />
          <SidebarDetail />
        </div>
      </div>
    </section>
  );
}
