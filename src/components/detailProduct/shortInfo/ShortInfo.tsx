import styles from './shortInfo.module.css';

export default function ShortInfo() {
  return (
    <section className={styles.shortInfo}>
      <div>
        <span className={styles.title}>Ngày đăng</span>
        <span className={styles.value}>31/07/2024</span>
      </div>
      <div>
        <span className={styles.title}>Ngày hết hạn</span>
        <span className={styles.value}>31/07/2024</span>
      </div>
      <div>
        <span className={styles.title}>Loại tin</span>
        <span className={styles.value}>Tin thường</span>
      </div>
    </section>
  );
}
