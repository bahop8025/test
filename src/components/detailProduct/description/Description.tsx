import styles from './description.module.css';

export default function Description() {
  return (
    <section className={styles.description}>
      <span>
        Quý vị đang xem nội dung tin rao "Giảm 500tr, bán 1984m2 đất mặt tiền Ấp Hòa Quới, xã Tân Chánh, Cần Đước, Long
        An, còn 2.5 tỷ, HH 3%" - Mã tin 40403074. Mọi thông tin, nội dung liên quan tới tin rao này là do người đăng tin
        đăng tải và chịu trách nhiệm. Batdongsan.com.vn luôn cố gắng để các thông tin được hữu ích nhất cho quý vị tuy
        nhiên Batdongsan.com.vn không đảm bảo và không chịu trách nhiệm về bất kỳ thông tin, nội dung nào liên quan tới
        tin rao này. Trường hợp phát hiện nội dung tin đăng không chính xác, Quý vị hãy thông báo và cung cấp thông tin
        cho Ban quản trị Batdongsan.com.vn theo Hotline 19001881 để được hỗ trợ nhanh và kịp thời nhất.
      </span>
    </section>
  );
}
