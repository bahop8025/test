import Maps from '../../maps/Maps';
import styles from './mapDetails.module.css';

export default function MapDetails() {
  return (
    <section className={styles.MapDetails}>
      <h3>Xem trên bản đồ</h3>
      <Maps />
    </section>
  );
}
