import SwipersFreemode from '../swipersFreemode/SwipersFreemode';
import styles from './productYou.module.css';

export default function ProductYou() {
  return (
    <section className={styles.productYou}>
      <h3>Bất động sản dành cho bạn</h3>
      <div>
        <SwipersFreemode />
      </div>
    </section>
  );
}
