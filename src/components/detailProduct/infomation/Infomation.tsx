import { Link } from 'react-router-dom';
import styles from './infomation.module.css';
import Buttons from '../../filed/Buttons';

export default function Infomation() {
  return (
    <section className={styles.infomation}>
      <div className={styles.agentInfo}>
        <Link to="">
          <img src="../../../../public/images/product.jpg" alt="" />
          <p>Đào Bá Hợp</p>
          <span>Xem thêm 144 tin khác</span>
        </Link>
      </div>
      <div className={styles.contactInfo}>
        <Buttons texts="Chat qua zalo" />
        <Buttons texts="Gửi Email" />
        <Buttons texts="Yêu cầu liên hệ lại" />
      </div>
    </section>
  );
}
