import styles from './searchs.module.css';
import { Input } from 'antd';
const { Search } = Input;

export default function Searchs() {
  return <Search placeholder="Search..." loading className={styles.searchs} />;
}
