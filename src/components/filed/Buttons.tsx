import { Button } from 'antd';
import styles from './buttons.module.css';

export default function Buttons(props) {
  const { texts, types, icons } = props;

  return (
    <Button className={styles.btn} type={types}>
      {texts} {icons && icons}
    </Button>
  );
}
