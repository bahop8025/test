import { Input } from 'antd';
import styles from './inputs.module.css';

export default function Inputs(props) {
  const {
    errors,
    value,
    id,
    name,
    onChange,
    touched,
    placeholder,
    labels,
    customClass,
    required,
    disabled = false
  } = props;

  return (
    <div className={`${styles.inputs} ${customClass}`}>
      {labels && (
        <label htmlFor="label">
          {labels}: {required && <span>*</span>}
        </label>
      )}
      <div>
        <Input
          id={id}
          onChange={onChange}
          status={touched && errors ? 'error' : ''}
          value={value}
          name={name}
          placeholder={placeholder}
          disabled={disabled}
        />
        {errors && <span>{errors}</span>}
      </div>
    </div>
  );
}
