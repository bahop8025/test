import { Select } from 'antd';
import styles from './selects.module.css';

export default function Selects(props) {
  const { labels, required, customerClass } = props;

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };

  return (
    <div className={`${styles.selects} ${customerClass}`}>
      {labels && (
        <label htmlFor="">
          {labels}: {required && <span>*</span>}
        </label>
      )}
      <Select
        defaultValue="lucy"
        style={{ width: '100%' }}
        onChange={handleChange}
        options={[
          { value: 'jack', label: 'Jack' },
          { value: 'lucy', label: 'Lucy' },
          { value: 'Yiminghe', label: 'yiminghe' },
          { value: 'disabled', label: 'Disabled', disabled: true }
        ]}
      />
    </div>
  );
}
