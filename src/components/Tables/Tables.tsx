import DataTable from 'react-data-table-component';
import Paginations from '../Paginations/Paginations';

const estilos = {
  headCells: {
    style: {
      backgroundColor: '#278a7c ',
      fontWeight: 'bold',
      fontSize: 14,
      color: '#fff'
    }
  },
  rows: {
    style: {
      fontSize: '14px',
      backgroundColor: '#fff',
      '&:hover': {
        backgroundColor: '#e1e1e1'
      }
    }
  },
  table: {
    style: {
      borderRadius: '10px',
      overflow: 'hidden'
    }
  }
};

export default function Tables(props) {
  const { datas, columns, handlePage, isSelectableRows = true, isPersistTableHead = false, setSelectedRows } = props;
  const { current_page, per_page, total } = datas;

  const handleSelectedRowsChange = (selectedRows) => {
    setSelectedRows && setSelectedRows(selectedRows);
  };

  const noDataComponent = <span style={{ padding: '20px' }}>{'noDataComponent'}</span>;

  return (
    <div>
      <DataTable
        //show column name khi table không có data
        persistTableHead={isPersistTableHead}
        // show text khi row không có data
        // noDataComponent={noDataComponent}
        columns={columns}
        //data truyền vao
        data={[]}
        // ẩn hiển slect all
        selectableRows={isSelectableRows}
        onSelectedRowsChange={handleSelectedRowsChange}
        fixedHeader
        fixedHeaderScrollHeight="100%"
        highlightOnHover
        selectableRowsHighlight
        noHeader
        customStyles={estilos}
      />
      {total > 5 && <Paginations totals={10 ?? ''} />}
    </div>
  );
}
