import { Link } from 'react-router-dom';
import styles from './links.module.css';
import { PlusOutlined } from '@ant-design/icons';

export default function Links(props) {
  const { to, texts, ishowIcon } = props;
  return (
    <Link to={to} className={styles.links}>
      {ishowIcon && <PlusOutlined />} {texts}
    </Link>
  );
}
