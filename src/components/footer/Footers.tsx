import { Footer } from 'antd/es/layout/layout';
import styles from './footers.module.css';

export default function Footers() {
  return <Footer className={styles.footers}></Footer>;
}
