import * as Yup from 'yup';

export const useSchemasEmployee = () => {
  return Yup.object({
    name: Yup.string().required('Request name'),
    email: Yup.string().email('Invalid Email').required('Request email'),
    position: Yup.string().required('Request position'),
    department: Yup.string().required('Request department'),
    // mode: '',
    // level: '',
    joining_Date: Yup.string().required('Request department'),
    date_Of_Birth: Yup.string().required('Request department'),
    // last_Year: '',
    // this_Year: '',
    // PTO_Taken: '',
    // PTO: '',
    bank_name: Yup.string().required('Request bank name'),
    bank_number: Yup.string().required('Request bank number'),
    bank_branch: Yup.string().required('Request bank branch'),
    bank_holder: Yup.string().required('Request bank holder'),
    university: Yup.string().required('Request university'),
    highest_Degree: Yup.string().required('Request highest degree'),
    degree: Yup.string().required('Request degree'),
    Ranking: Yup.string().required('Request ranking'),
    Exps: Yup.string().required('Request exps'),
    major: Yup.string().required('Request major')
  });
};
